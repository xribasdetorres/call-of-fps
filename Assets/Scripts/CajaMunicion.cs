﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CajaMunicion : MonoBehaviour {

    public GameObject prefabMun;

    private float vida;

	// Use this for initialization
	void Start () {
        vida = 3;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    
    void OnCollisionEnter(Collision coll)
    {
        if(coll.collider.tag == "RockWeap")
        {

            
            if (vida > 0)
            {
                vida -= 1;
            }
            if (vida <= 0)
            {
                Instantiate(prefabMun, this.transform.position, this.transform.rotation);
                DestroyObject(gameObject);
            }
        }
    }
}
