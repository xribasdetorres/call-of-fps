﻿using System.Collections; 
using System.Collections.Generic;
using UnityEngine;

public class RuscoWeponController : MonoBehaviour {

    public float maxAmmo;
    public GameObject prefabRusco;

    private float currentAmmo;
    private float RechargeAmmo;
    private float AbleAmmo;
    private float bulletImpulse = 20f;
    



	// Use this for initialization
	void Start () {
        currentAmmo = maxAmmo;
        RechargeAmmo += currentAmmo / 2;
        AbleAmmo += currentAmmo / 2;
        

	}
	
	// Update is called once per frame
	void Update () {
        currentAmmo = AbleAmmo + RechargeAmmo;
        AbleAmmo = Mathf.Round(AbleAmmo);
        RechargeAmmo = Mathf.Round(RechargeAmmo);
        
	}
    public void Shoot()
    {
        if (AbleAmmo > 0) { 
            AbleAmmo -= 1;
            GameObject bullet = (GameObject)Instantiate(prefabRusco, Camera.main.transform.position + Camera.main.transform.forward, Camera.main.transform.rotation);
            bullet.GetComponent<Rigidbody>().AddForce(Camera.main.transform.forward * bulletImpulse, ForceMode.Impulse);
        }
        if (AbleAmmo < 1)
        {
            if (RechargeAmmo > 1)
            {
                AbleAmmo += 5;
                RechargeAmmo -= 5;
            }
        }
    }
    public void Recharge()
    {
        if (AbleAmmo < 1)
        {
            if (RechargeAmmo < 1)
            {
                AbleAmmo += 5;
                RechargeAmmo -= 5;
            }
        }
    }
    public void addAmmo()
    {
        if(currentAmmo < maxAmmo)
        {
            if(RechargeAmmo <= (maxAmmo/2))
            {
                RechargeAmmo += 5;
            }
            else
            {
                AbleAmmo += 5;
            }
        }
    }
    public float getRechargeAmmo()
    {
        return RechargeAmmo;
    }
    public float getAbleAmmo()
    {
        return AbleAmmo;
    }
}
