﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemigoController: MonoBehaviour
{

    private NavMeshAgent agent;
    

    [Header("Vida Personaje")]
    public int life = 5;

    [Header("Ruta enemigo")]
    public GameObject[] PuntosPatrollador;
    public int PuntoInicial;
    private int Puntos;

    [Header("Minas rusco")]
    public GameObject PrefabMina;
    public float TiempoThrowBomba;
    private float throwBomba;
    private Vector3 PosBomba;

    private AudioSource[] Sonidos;
    private float tiempoGrun;

    // Use this for initialization
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        Sonidos = GetComponents<AudioSource>();
        Puntos = PuntoInicial;
        agent.SetDestination(PuntosPatrollador[Puntos].transform.position);
        throwBomba = TiempoThrowBomba;
        tiempoGrun = 4;
    }

    // Update is called once per frame
    void Update()
    {
        
        throwBomba -= Time.deltaTime;
        tiempoGrun -= Time.deltaTime;
        if(tiempoGrun <= 0)
        {
            Grunido();
            tiempoGrun = 4; 
        }
        if (agent.remainingDistance <= 0.1f)
        {
            Puntos++;
            if (Puntos > PuntosPatrollador.Length)
            {
                Puntos = 0;
            }
            agent.SetDestination(PuntosPatrollador[Puntos].transform.position);
        }
        if(throwBomba <= 0)
        {
            PosBomba.Set(this.transform.position.x, this.transform.position.y + 0.5f, this.transform.position.z);
            Instantiate(PrefabMina, PosBomba, this.transform.rotation);
            throwBomba = TiempoThrowBomba;
        }

    }
    private void FixedUpdate()
    {
        
        
    }

   
    

    public void Damage(int hit)
    {
        
        
        life -= hit;
        if(life <= 0)
        {
            Muerto();
           
            DestroyObject(gameObject);
        }
    }

    void OnCollisionEnter(Collision other)
    {
        
        if (other.gameObject.tag == "RockWeap")
        {
            Damage(5);
        }
        
    }
    void OnTriggerEnter(Collider coll)
    {
        
        if (coll.tag == "Abejas")
        {
           
            Damage(2);
        }
    }
    void OnTriggerStay(Collider coll)
    {
        
        if (coll.tag == "Abejas")
        {
            
            Damage(2);
        }
    }
    void Grunido()
    {
        Sonidos[0].Play();
    }
    void Muerto()
    {
        Sonidos[1].Play();
       
    }
}
