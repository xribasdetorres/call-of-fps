﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EscudoItemCont : MonoBehaviour {

    public GameObject EscudoCrear;
    public float Posx, Posy, Posz, Rotx, Roty, Rotz;
    public float timeSpawn;

    private GameObject EscudoItem;
    private GameObject Player;
    private Statsplayer statsPlayer;
    private Vector3 PosEscudoObject;
    private Quaternion RotEscudoObject;
    private float time;

	// Use this for initialization
	void Start () {
        EscudoItem = GameObject.FindGameObjectWithTag("EscudoObj");
        Player = GameObject.Find("Personaje");
        statsPlayer = Player.GetComponent<Statsplayer>();
        PosEscudoObject.Set(Posx, Posy, Posz);
        RotEscudoObject.Set(Rotx, Roty, Rotz, 0);
        time = timeSpawn;
	}
	
	// Update is called once per frame
	void Update () {
        
		if(EscudoItem == null)
        {
            
            time -= Time.deltaTime;
            if (time < 0)
            {
                Instantiate(EscudoCrear, PosEscudoObject, RotEscudoObject);
                EscudoItem = GameObject.FindGameObjectWithTag("EscudoObj");
                time = timeSpawn;
            }
        }
	}
    void OnTriggerEnter(Collider coll)
    {
        
        if (coll.tag == "Player")
        {
            Debug.Log("Hellowww2w");
            if(EscudoItem != null)
            {
                Debug.Log("Hellow");
                statsPlayer.masEscudo();
                Destroy(EscudoItem);
            }
        }
    }
}
