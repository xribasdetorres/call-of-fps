﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Statsplayer : MonoBehaviour {

    private float vida;
    private float escudo;

	// Use this for initialization
	void Start () {
        vida = 300;
        escudo = 0;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public float getVida()
    {
        return vida;
    }

    public float getEscudo()
    {
        return escudo;
    }
    
    public void masEscudo()
    {
        if(escudo < 100)
        {
            escudo += 10;
        }
        
    }

    public void Daño(int daño)
    {
        if(escudo > 0)
        {
            escudo -= daño;
        }
        else
        {
            if(vida > 0)
            {
                vida -= daño;
            }
            
        }
        if(vida <= 0)
        {

            SceneManager.LoadScene("GameOver");
        }
    }
    
}
