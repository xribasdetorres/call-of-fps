﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DañoAbejas : MonoBehaviour {

    public float duration;

    private GameObject Player;
    private Statsplayer InfoPlayer;

    

    // Use this for initialization
    void Start () {
        Player = GameObject.Find("Personaje");
        InfoPlayer = Player.GetComponent<Statsplayer>();
     }
	
	// Update is called once per frame
	void Update () {
        duration -= Time.deltaTime;
        if(duration <= 0)
        {
            DestroyObject(gameObject);
        }
        
	}
    void OnTriggerEnter(Collider coll)
    {
        
       
        if(coll.tag == "Player")
        {
            InfoPlayer.Daño(1);
        }
    }
    void OnTriggerStay(Collider coll)
    {

        if (coll.tag == "Player")
        {
            InfoPlayer.Daño(1);
        }
    }
    
}
