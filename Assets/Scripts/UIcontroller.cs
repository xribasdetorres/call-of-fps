﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIcontroller : MonoBehaviour {

    public GameObject Player;
    public GameObject Vida;
    public GameObject AmmoAct;
    public GameObject AmmoRec;
    public GameObject Escudo;
    public GameObject Arma;
    public Sprite Piedras;
    public Sprite Ruscos;


    private TextMeshProUGUI Vidatxt;
    private TextMeshProUGUI escudotxt;
    private TextMeshProUGUI ammoActtxt;
    private TextMeshProUGUI ammoRectxt;
    private Statsplayer playerinfo;
    private RuscoWeponController rwpc;
    private Image imgArma;
    private Weaponcontroller wpc;
    private string weapon;

    
	// Use this for initialization
	void Start () {
        
        playerinfo = Player.GetComponent<Statsplayer>();
        Vidatxt = Vida.GetComponent<TextMeshProUGUI>();
        ammoActtxt = AmmoAct.GetComponent<TextMeshProUGUI>();
        ammoRectxt = AmmoRec.GetComponent<TextMeshProUGUI>();
        escudotxt = Escudo.GetComponent<TextMeshProUGUI>();
        wpc = Player.GetComponent<Weaponcontroller>();
        rwpc = Player.GetComponent<RuscoWeponController>();
        imgArma = Arma.GetComponent<Image>();
    }
	
	// Update is called once per frame
	void Update () {
        weapon = wpc.getWeapon();
        Vidatxt.SetText(playerinfo.getVida().ToString());
        escudotxt.SetText(playerinfo.getEscudo().ToString());
        if(weapon == "piedras")
        {
            imgArma.sprite = Piedras;
            ammoActtxt.SetText("Infinito");
            ammoRectxt.SetText("Infinito");
        }else
        {
            imgArma.sprite = Ruscos;
            ammoActtxt.SetText(rwpc.getAbleAmmo().ToString());
            ammoRectxt.SetText(rwpc.getRechargeAmmo().ToString());
        }
	}
}
