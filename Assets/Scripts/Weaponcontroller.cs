﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weaponcontroller : MonoBehaviour {

    public GameObject piedraPrefab;
    

    private string ActualArma;
    private float bulletImpulse = 20f;
    private GameObject Player;
    private RuscoWeponController rwpc;


	// Use this for initialization
	void Start () {
        
        ActualArma = "piedras";
        Player = GameObject.FindGameObjectWithTag("Player");
        rwpc = Player.GetComponent<RuscoWeponController>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void Shoot()
    {
        if(ActualArma == "piedras")
        {
            GameObject bullet = (GameObject)Instantiate(piedraPrefab, Camera.main.transform.position + Camera.main.transform.forward, Camera.main.transform.rotation);
            bullet.GetComponent<Rigidbody>().AddForce(Camera.main.transform.forward * bulletImpulse, ForceMode.Impulse);
        }
        else
        {
            rwpc.Shoot();
        }
    }
    public void change()
    {
        if (ActualArma == "piedras")
        {
            ActualArma = "ruscos";
        }
        else
        {
            ActualArma = "piedras";
        }
    }
    public string getWeapon()
    {
        return ActualArma;
    }
}
