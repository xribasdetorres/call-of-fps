﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuControl : MonoBehaviour {

    private AudioSource Music;

	// Use this for initialization
	void Start () {
        Music = GetComponent<AudioSource>();
        Music.Play();
    }
	
	// Update is called once per frame
	void Update () {
		
      
        
    }
    public void Play()
    {
        SceneManager.LoadScene("Level1");
    }
    public void Menu()
    {
        SceneManager.LoadScene("Menu");
    }
    
}
