﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RuscoController : MonoBehaviour {

    public GameObject prebafAbejas;

    private Vector3 PositionAbejas;

        // Use this for initialization
	void Start () {
       
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnCollisionEnter(Collision coll)
    {
        PositionAbejas.Set(this.transform.position.x, this.transform.position.y + 2, this.transform.position.z);
        if (gameObject.tag == "RuscoWeap")
        {
            
            Instantiate(prebafAbejas, PositionAbejas, this.transform.rotation);
            DestroyObject(gameObject);
        }
        if (gameObject.tag == "RuscoMina")
        {
           
            if (coll.collider.tag == "PlayerObj")
            {
                
                Instantiate(prebafAbejas, PositionAbejas, this.transform.rotation);
                DestroyObject(gameObject);
            }    
        }
    }
}
