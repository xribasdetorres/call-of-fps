﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Municion : MonoBehaviour {

    private GameObject Player;
    private RuscoWeponController RuscoMun;
    private float MunTotal;

	// Use this for initialization
	void Start () {
        
        Player = GameObject.FindGameObjectWithTag("Player");
        RuscoMun = Player.GetComponent<RuscoWeponController>();
        MunTotal = RuscoMun.getAbleAmmo() + RuscoMun.getRechargeAmmo();
    }
	
	// Update is called once per frame
	void Update () {
        MunTotal = RuscoMun.getAbleAmmo() + RuscoMun.getRechargeAmmo();
    }

    void OnTriggerEnter(Collider coll)
    {
        
        if (coll.tag == "Player")
        {
           if(MunTotal < 7) { 
            RuscoMun.addAmmo();
            DestroyObject(gameObject);
           }
        }
    }
}
