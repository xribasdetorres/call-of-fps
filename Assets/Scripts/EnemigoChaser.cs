﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemigoChaser : MonoBehaviour {

    public enum State { Idle, Patrol, Chase, Explode, Dead };
    public State state;

    private NavMeshAgent agent;
    private Animator anim;
   // private SoundPlayer sound;

    [Header("Enemigo vida")]
    public int life = 5;

    [Header("Detecion del jugador")]
    public float radius;
    public float idleRadius;
    public float chaseRadius;
    public LayerMask targetMask;
    public bool targetDetected = false;
    private Transform targetTransform;

    [Header("Patrol path")]
    public bool stopAtEachNode = true;
    public float timeStopped = 1.0f;
    private float timeCounter = 0.0f;
    public Transform[] pathNodes;
    private int currentNode = 0;
    private bool nearNode = false;

    private GameObject Player;
    private Statsplayer Playervida;

    private AudioSource[] Sonidos;
    private float tiempoGrun;

    // Use this for initialization
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        Sonidos = GetComponents<AudioSource>();
        tiempoGrun = 4;

        Player = GameObject.Find("Personaje");
        Playervida = Player.GetComponent<Statsplayer>();
        

        nearNode = true;
        SetIdle();
    }

    // Update is called once per frame
    void Update()
    {

        tiempoGrun -= Time.deltaTime;
        if (tiempoGrun <= 0)
        {
            Grunido();
            tiempoGrun = 4;
        }
        switch (state)
        {
            case State.Idle:
                Idle();
                break;
            case State.Patrol:
                Patrol();
                break;
            case State.Chase:
                Chase();
                break;
            
            case State.Dead:
                Dead();
                break;
            default:
                break;
        }
    }
    private void FixedUpdate()
    {
        targetDetected = false;
        Collider[] cols = Physics.OverlapSphere(this.transform.position, radius, targetMask);
        if (cols.Length != 0)
        {
            targetDetected = true;
            targetTransform = cols[0].transform;
        }
    }

    void Idle()
    {
        if (targetDetected)
        {
            SetChase();
            return;
        }

        if (timeCounter >= timeStopped)
        {
            if (nearNode) GoNearNode();
            else GoNextNode();
            SetPatrol();
        }
        else timeCounter += Time.deltaTime;
    }
    void Patrol()
    {
        if (targetDetected)
        {
            SetChase();
            return;
        }

        if (agent.remainingDistance <= 0.1f)
        {
            if (stopAtEachNode) SetIdle();
            else GoNextNode();
        }
    }
    void Chase()
    {
        if (!targetDetected)
        {
            nearNode = true;
            SetIdle();
            return;
        }
        agent.SetDestination(targetTransform.position);

        


    }
    void Explode() { }
    void Dead() { }

    void SetIdle()
    {
        agent.isStopped = true;
        //anim.SetBool("isMoving", false);
        radius = idleRadius;
        timeCounter = 0;

        int random = Random.Range(0, 4);
        //sound.Play(random, 1);

        state = State.Idle;
    }
    void SetPatrol()
    {
        agent.isStopped = false;
        agent.stoppingDistance = 0;
        radius = idleRadius;
       // anim.SetBool("isMoving", true);

        state = State.Patrol;
    }
    void SetChase()
    {
        agent.isStopped = false;
        agent.SetDestination(targetTransform.position);
        agent.stoppingDistance = 2.0f;
      //  anim.SetBool("isMoving", true);
        radius = chaseRadius;

        state = State.Chase;
    }
   
    void SetDead()
    {
       // sound.Play(8, 1);
        Destroy(this.gameObject);
        state = State.Dead;
    }

    void GoNextNode()
    {
        currentNode++;
        if (currentNode >= pathNodes.Length) currentNode = 0;

        agent.SetDestination(pathNodes[currentNode].position);
    }
    void GoNearNode()
    {
        nearNode = false;
        float minDistance = Mathf.Infinity;
        for (int i = 0; i < pathNodes.Length; i++)
        {
            if (Vector3.Distance(transform.position, pathNodes[i].position) < minDistance)
            {
                minDistance = Vector3.Distance(transform.position, pathNodes[i].position);
                currentNode = i;
            }
        }
        agent.SetDestination(pathNodes[currentNode].position);
    }

    
    

    public void Damage(int hit)
    {

        if (state == State.Dead) return;
        life -= hit;
        if (life <= 0) SetDead();
    }

    void OnCollisionEnter(Collision other)
    {
        
        if (other.gameObject.tag == "RockWeap")
        {
            
            Damage(1);
        }
        
    }
    void OnTriggerEnter(Collider coll)
    {
        
        if (coll.tag == "Player")
        {
            Playervida.Daño(2);
        }
        if (coll.tag == "Abejas")
        {
    
            Damage(2);
        }
    }
    void OnTriggerStay(Collider coll)
    {

        if (coll.tag == "Player")
        {
            Playervida.Daño(2);
        }
        if (coll.tag == "Abejas")
        {
            
            Damage(2);
        }
    }
    void Grunido()
    {
        Sonidos[0].Play();
    }
    void Muerto()
    {
        Sonidos[1].Play();

    }
}
