﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameInputManager : MonoBehaviour {

    private Playercontrol playerController;
    private float sensitivity = 3.0f;
    private Vistacontrol lookRotation;
    private MouseCursor mouseCursor;
    private Weaponcontroller wpCont;
    private AudioSource Music;
    

    // Use this for initialization
    void Start () {
        playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<Playercontrol>();
        lookRotation = playerController.GetComponent<Vistacontrol>();
        wpCont = playerController.GetComponent<Weaponcontroller>();
        Music = GetComponent<AudioSource>();

        mouseCursor = new MouseCursor();
        mouseCursor.HideCursor();
        Music.Play();
    }
	
	// Update is called once per frame
	void Update () {
        //El movimiento del player
        Vector2 inputAxis = Vector2.zero;
        inputAxis.x = Input.GetAxis("Horizontal");
        inputAxis.y = Input.GetAxis("Vertical");
        playerController.SetAxis(inputAxis);
        //El salto del player
        if (Input.GetButton("Jump"))
        {
            Debug.Log("Salta");
            playerController.StartJump();
        }

        //Rotación de la cámara
        Vector2 mouseAxis = Vector2.zero;
        mouseAxis.x = Input.GetAxis("Mouse X") * sensitivity;
        mouseAxis.y = Input.GetAxis("Mouse Y") * sensitivity;
        //Debug.Log("Mouse X = " + mouseAxis.x + " Y = " + mouseAxis.y);
        lookRotation.SetRotation(mouseAxis);

        //Cursor del ratón
        if (Input.GetMouseButtonDown(0)) mouseCursor.HideCursor();
        else if (Input.GetKeyDown(KeyCode.Escape)) mouseCursor.ShowCursor();


        if (Input.GetKeyDown(KeyCode.C))
        {
            wpCont.change();
        }

        if(Input.GetMouseButtonDown(0))
        {
            wpCont.Shoot();
        }
        //if(Input.GetKeyDown(KeyCode.R)) laser.Reload();

        //if (Input.GetMouseButtonDown(0)) ballShoot.Shot();
    }
}
